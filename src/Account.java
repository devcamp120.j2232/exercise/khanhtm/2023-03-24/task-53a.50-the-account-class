public class Account {
 /**
  * Các thuộc tính
  */
 private String id;
 private String name;
 private int balance = 0;
 /**
  * Phương thức khởi tạo
  */
 public Account (String id,String name){
  this.id=id;
  this.name=name;
 }

 public Account (String id,String name,int balance){
  this.id=id;
  this.name=name;
  this.balance=balance;
 }
  /**
  * Getter method
  * @return
  */
  public String getId() {
   return this.id;
  }

  public String getName() {
   return this.name;
  }

  public int getBalance() {
   return this.balance;
  }
  /**
   * Setter method
   * @return
  */
  public int credit( int amount){
   this.balance += amount;
   return this.balance;
  }

  public int debit( int amount){
   if (amount <= this.balance) {
   this.balance  = balance - amount;
   } else{
    System.out.println("Amount execeeded balance");
   }
   return this.balance;
  }

  public int tranferTo (Account Account, int amount){
   if (amount <= this.balance) {
    Account.balance += amount;
    this.balance = this.balance - amount;
    } else{
     System.out.println("Amount execeeded balance");
    }
    return this.balance;
  }

  public String toString(){
   return "Account [id ="+ this.id +", name-"+this.name+", balance="+this.balance + "]";
  }
}
